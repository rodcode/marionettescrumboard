var ModalModule = (function(views){
    'use strict';
    
    var ModalView = Backbone.View.extend({
        tagName: 'div',
        className: 'modal fade',
        attributes:{ tabindex: -1, role: 'dialog'},
        id: 'story-form',
        events:{
            'click .save' : '_accept'
        },
        initialize: function(options){
            this._options = options;
            this.$el.on('hidden.bs.modal',this.remove.bind(this));
            this.render();
        },
        render: function(){
            this.$el.html(this._options.template);
        },
        show : function(){
            this.$el.modal('show');
        },
        _accept: function(){
            this.trigger('accept-modal');
            this.$el.modal('hide');
        }
    });

    views.ModalView = ModalView;

    return views;

})((ModalModule || {}));