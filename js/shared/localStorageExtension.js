var BackboneLocalStorage = (function(myModule){    
    'use strict';
            
    function _getConstructorLocalStorage(modelType){
        return function (){
            Backbone[modelType].apply(this, arguments);
            if(this.localStorageNamespace){
                this.localStorage = new Backbone.LocalStorage(this.localStorageNamespace);
            }
        };
    }

    myModule.Model = Backbone.Model.extend({
        constructor: _getConstructorLocalStorage('Model')
    }); 

    myModule.Collection = Backbone.Collection.extend({
        constructor: _getConstructorLocalStorage('Collection')
    });

    return myModule; 

})((BackboneLocalStorage || {}));