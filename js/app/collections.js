var Collections = (function (collections) {

    'use strict';

    var BoardSet = BackboneLocalStorage.Collection.extend({
        model: Models.Board,
        url: 'api/board',
        localStorageNamespace: 'boardStorage'
    });

    var StorySet = BackboneLocalStorage.Collection.extend({
        model: Models.Story,
        url: 'api/story',
        comparator: function (story) {
            return story.get('order');
        },
        localStorageNamespace: 'storyStorage'
    });

    var StageSet = BackboneLocalStorage.Collection.extend({
        url: 'api/stage',
        model: Models.Stage,
        localStorageNamespace: 'stageStorage'
    });

    collections.StageSet = StageSet;
    collections.StorySet = StorySet;
    collections.BoardSet = BoardSet;

    return collections;

})((Collections || {}));