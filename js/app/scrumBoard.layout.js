var ScrumBoard = (function (module) {
    'use strict';

    module.RootLayout = Mn.View.extend({
        el: '#scrumapp',
        regions: {
            main: '#app',
            header: '#header'
        }
    });

    return module;

})((ScrumBoard || {}));