var ScrumBoard = (function (module) {

    'use strict';

    var StoryItemView = Mn.View.extend({
        tagName: 'li',
        className: 'story',
        template: '#storyTemplate',
        events: {
            'click div.story-item': 'edit_story'
        },
        edit_story: function () {
            var template = _.template($('#storyForm').html());
            var modal = new ModalModule.ModalView({ template: template(this.model.toJSON()) });
            modal.show();
            modal.on('accept-modal', this.updateModels, this);
        },
        updateModels: function () {
            var data = {};
            var form_data = $('#story-form form').serializeArray();
            for (var i in form_data) {
                data[form_data[i].name] = form_data[i].value;
            }
            this.model.save(data);
            this.render();
        },
        onRender: function () {
            this.$el.data('model', this.model);
        }
    });

    var StoryCollectionView = Mn.CollectionView.extend({
        tagName: 'ul',
        className: 'story-set',
        childView: StoryItemView,
        initialize: function (attributes, options) {
            this.stageId = options.stageId;
            this.$el.sortable({
                items: 'li',
                connectWith: '.story-set',
                remove: this.remove.bind(this),
                receive: this.receive.bind(this),
                update: this.update.bind(this),
            });
        },
        remove: function (event, ui) {
            var modelAttributes = $(ui.item).data('model');
            var model = this.collection.remove(modelAttributes.id, {silent: true});
        },
        receive: function (event, ui) {
            var modelStory = $(ui.item).data('model');
            modelStory.set('stageId', this.stageId);
            modelStory.save();
            this.collection.add(modelStory,{silent: true});
        },
        update: function () {
            this.$('.story').each(function (index, item) {
                var modelStory = this.collection.findWhere({ id: $(item).data('model').id });
                if (modelStory) {
                    modelStory.save({ order: index }, { silent: true });
                }
            }.bind(this));
        },
        onRenderChildren: function () {
            this.$el.append('<li class="drop"></li>');
        }
    });

    var StageItemView = Mn.View.extend({
        template: '#stageTemplate',
        className: 'stage',
        regions: {
            stories: {
                el: '.posits-story'
            }
        },
        events: {
            'click .new': 'new_story'
        },
        initialize: function () {
            this.collection = new Collections.StorySet();
            this.collection.fetch();
            var filtered = this.collection.where({ stageId: this.model.get('id') });
            this.collection.reset(filtered);
        },
        new_story: function () {
            var story = new Models.Story();
            story.save({ stageId: this.model.get('id'), order: this.collection.length - 1 });
            this.getRegion('stories').reset();
            this.collection.add(story);           
            this.showCollectionView(); 
        },
        showCollectionView: function(){
            this.showChildView('stories', new StoryCollectionView(
                {
                    collection: this.collection
                },
                {
                    stageId: this.model.get('id')
                }
            ));
        },
        onRender: function () {
           this.showCollectionView();
        }
    });

    var StageView = Mn.CollectionView.extend({
        id: 'stages',
        childView: StageItemView,
        filter: function (child) {
            return child.get('boardId') === this.model.get('id');
        }
    });

    var BoardItemView = Mn.View.extend({
        tagName: 'li',
        template: '#boardItemTemplate',
        attributes: function () {
            return {
                class: this.model.get('active') ? 'active' : 'no-active'
            };
        },
        events: {
            'click a': 'initStages'
        },
        initialize: function () {
            var mainRegion = ScrumBoard.App.root.getRegion('main');
            if (this.model.get('active')) {
                mainRegion.reset();
                this.initStages();
            }
        },
        initStages: function () {
            var stages = new Collections.StageSet();
            ScrumBoard.App.root.showChildView('main', new StageView({
                collection: stages,
                model: this.model
            }));
            stages.fetch();
        }
    });

    var BoardsCollectionView = Mn.CollectionView.extend({
        tagName: 'ul',
        id: 'board-list',
        className: 'nav nav-tabs',
        childView: BoardItemView
    });

    var BoardsView = Mn.View.extend({
        template: '#templateHeader',
        regions: {
            listBoard: {
                el: '#board-items'
            }
        },
        ui: {
            destroy: '.destroy',
            change: '.change',
            new: '.new'
        },
        events: {
            'click @ui.destroy': 'destroy',
            'click @ui.change': 'change',
            'click @ui.new': 'new_board'
        },
        change: function () {
            var model = this.getActivePanel();
            var title = window.prompt('Nombre para el tablero ', model.get('title'));
            if (title) {
                model.save({ 'title': title });
            }
            this.render();
        },
        destroy: function () {
            if (window.confirm('¿Desea eliminar el tablero?')) {
                var model = this.getActivePanel();
                model.destroy();
                window.location.href = '/';
            }
        },
        new_board: function () {
            var board_title = window.prompt('Nombre para el tablero');
            if (board_title) {
                var boardModel = this.collection.create({ 'title': board_title }, { wait: true });
                this.createStages(boardModel.get('id'));
            }
        },
        onRender: function () {
            this.showChildView('listBoard', new BoardsCollectionView({
                collection: this.collection
            }));
        },
        getActivePanel: function () {
            return this.collection.findWhere({ active: true });
        },
        makeBoardActive: function (id) {
            this.collection.invoke('set', { active: false });
            var activeBoard = this.collection.findWhere({ id: id });
            activeBoard.set('active', true);
            this.render();
        },
        createStages: function (id) {
            var stages = new Collections.StageSet();
            stages.create({ boardId: id, title: 'TODO' });
            stages.create({ boardId: id, title: 'IN PROGRESS' });
            stages.create({ boardId: id, title: 'DONE' });
            return stages;
        }
    });

    module.BoardsView = BoardsView;

    return module;

})((ScrumBoard || {}));