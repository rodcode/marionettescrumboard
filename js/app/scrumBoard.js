(function () {
    'use strict';

    ScrumBoard.App.on('start', function () {
        var controller = new ScrumBoard.Controller();
        controller.router = new ScrumBoard.Router({
            controller: controller
        });

        controller.start();
        Backbone.history.start();
    });

    ScrumBoard.App.start();

})();