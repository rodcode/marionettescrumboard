var ScrumBoard = (function (module) {
    'use strict';

    var ScrumBoardApp = Mn.Application.extend({
        setRootLayout: function () {
            this.root = new ScrumBoard.RootLayout();
        }
    });

    module.App = new ScrumBoardApp();

    module.App.on('before:start', function () {
        module.App.setRootLayout();
    });

    return module;

})((ScrumBoard || {}));