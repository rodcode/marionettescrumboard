var Models = (function (models) {

    'use strict';

    var Board = BackboneLocalStorage.Model.extend({
        urlRoot: 'api/board',
        localStorageNamespace: 'boardStorage'
    });

    var Story = BackboneLocalStorage.Model.extend({
        defaults: {
            description: '',
            color: 'yellow',
            order: 0,
            duration: 0
        },
        urlRoot: 'api/story',
        localStorageNamespace: 'storyStorage'
    }); 

    var Stage = BackboneLocalStorage.Model.extend({
        urlRoot: 'api/stage',
        localStorageNamespace: 'stageStorage'
    });

    models.Stage = Stage;
    models.Story = Story;
    models.Board = Board;

    return models;

})((Models || {}));