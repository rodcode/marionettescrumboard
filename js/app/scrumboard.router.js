var ScrumBoard = (function (module) {
    'use strict';

    module.Router = Mn.AppRouter.extend({
        appRoutes: {
            'board/:id': 'board'
        }
    });

    module.Controller = Mn.Object.extend({
        initialize: function () {
            this.boardsList = new Collections.BoardSet();
        },
        start: function () {
            this.showScrumBoards(this.boardsList);
            this.boardsList.fetch();
        },
        showScrumBoards: function (boardList) {
            ScrumBoard.App.root.showChildView('header', new ScrumBoard.BoardsView({
                collection: boardList
            }));
        },
        board: function (id) {
            var headerView = ScrumBoard.App.root.getChildView('header');
            headerView.makeBoardActive(id);
        }
    });

    return module;

})((ScrumBoard || {}));